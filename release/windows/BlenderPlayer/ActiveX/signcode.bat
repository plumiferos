rem
rem $Id: signcode.bat 255 2003-01-01 15:06:10Z hos $
rem
rem ***** BEGIN GPL/BL DUAL LICENSE BLOCK *****
rem
rem This program is free software; you can redistribute it and/or
rem modify it under the terms of the GNU General Public License
rem as published by the Free Software Foundation; either version 2
rem of the License, or (at your option) any later version. The Blender
rem Foundation also sells licenses for use in proprietary software under
rem the Blender License.  See http://www.blender.org/BL/ for information
rem about this.
rem
rem This program is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem GNU General Public License for more details.
rem
rem You should have received a copy of the GNU General Public License
rem along with this program; if not, write to the Free Software Foundation,
rem Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
rem
rem The Original Code is Copyright (C) 2001-2002 by NaN Holding BV.
rem All rights reserved.
rem
rem The Original Code is: all of this file.
rem
rem Contributor(s): none yet.
rem
rem ***** END GPL/BL DUAL LICENSE BLOCK *****
SignCode.exe -spc a:\mycredentials.spc -v a:\myprivatekey.pvk -n "Blender3DPlugin" -i http://www.blender.nl/plugin -$ commercial -t http://timestamp.verisign.com/scripts/timstamp.dll Blender3DPlugin.cab
/**
 * $Id: BKE_icons.h 6568 2006-01-28 18:33:28Z hos $
 *
 * ***** BEGIN GPL/BL DUAL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. The Blender
 * Foundation also sells licenses for use in proprietary software under
 * the Blender License.  See http://www.blender.org/BL/ for information
 * about this.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * The Original Code is Copyright (C) 2001-2002 by NaN Holding BV.
 * All rights reserved.
 *
 * The Original Code is: all of this file.
 *
 * Contributor(s): none yet.
 * 
 * ***** END GPL/BL DUAL LICENSE BLOCK *****
 */

#ifndef BKE_ICONS_H
#define BKE_ICONS_H

/*
 Resizable Icons for Blender
*/

typedef void (*DrawInfoFreeFP) (void *drawinfo);

struct Icon
{
	void *drawinfo;
	void *obj;
	short type;
	short changed;
	DrawInfoFreeFP drawinfo_free;
};

typedef struct Icon Icon;

void BKE_icons_init(int first_dyn_id);

/* return icon id for library object or create new icon if not found */
int	BKE_icon_getid(struct ID* id);

/* retrieve icon for id */
struct Icon* BKE_icon_get(int icon_id);

/* set icon for id if not already defined */
/* used for inserting the internal icons */
void BKE_icon_set(int icon_id, struct Icon* icon);

/* remove icon and free date if library object becomes invalid */
void BKE_icon_delete(struct ID* id);

/* report changes - icon needs to be recalculated */
void BKE_icon_changed(int icon_id);

/* free all icons */
void BKE_icons_free();


#endif /*  BKE_ICONS_H */

/**
 * blenlib/DNA_group_types.h (mar-2001 nzc)
 *	
 * $Id: DNA_group_types.h 8877 2006-11-14 17:16:15Z ton $ 
 *
 * ***** BEGIN GPL/BL DUAL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. The Blender
 * Foundation also sells licenses for use in proprietary software under
 * the Blender License.  See http://www.blender.org/BL/ for information
 * about this.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * The Original Code is Copyright (C) 2001-2002 by NaN Holding BV.
 * All rights reserved.
 *
 * The Original Code is: all of this file.
 *
 * Contributor(s): none yet.
 *
 * ***** END GPL/BL DUAL LICENSE BLOCK *****
 */
#ifndef DNA_GROUP_TYPES_H
#define DNA_GROUP_TYPES_H

#include "DNA_listBase.h"
#include "DNA_ID.h"

struct Object;

typedef struct GroupObject {
	struct GroupObject *next, *prev;
	struct Object *ob;
	void *lampren;		/* used while render */
	int recalc;			/* copy of ob->recalc, used to set animated groups OK */
	int pad;
} GroupObject;


typedef struct Group {
	ID id;
	
	ListBase gobject;	/* GroupObject */
	unsigned int layer;
	int pad;
} Group;


#endif


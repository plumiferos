/**
 * $Id: DNA_scene_types.h 12320 2007-10-21 15:42:08Z schlaile $ 
 *
 * ***** BEGIN GPL/BL DUAL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. The Blender
 * Foundation also sells licenses for use in proprietary software under
 * the Blender License.  See http://www.blender.org/BL/ for information
 * about this.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * The Original Code is Copyright (C) 2001-2002 by NaN Holding BV.
 * All rights reserved.
 *
 * The Original Code is: all of this file.
 *
 * Contributor(s): none yet.
 *
 * ***** END GPL/BL DUAL LICENSE BLOCK *****
 */
#ifndef DNA_SCENE_TYPES_H
#define DNA_SCENE_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DNA_vec_types.h"
#include "DNA_listBase.h"
#include "DNA_scriptlink_types.h"
#include "DNA_ID.h"
#include "DNA_scriptlink_types.h"

struct Radio;
struct Object;
struct World;
struct Scene;
struct Image;
struct Group;
struct bNodeTree;

typedef struct Base {
	struct Base *next, *prev;
	unsigned int lay, selcol;
	int flag;
	short sx, sy;
	struct Object *object;
} Base;

typedef struct AviCodecData {
	void			*lpFormat;  /* save format */
	void			*lpParms;   /* compressor options */
	unsigned int	cbFormat;	    /* size of lpFormat buffer */
	unsigned int	cbParms;	    /* size of lpParms buffer */

	unsigned int	fccType;            /* stream type, for consistency */
	unsigned int	fccHandler;         /* compressor */
	unsigned int	dwKeyFrameEvery;    /* keyframe rate */
	unsigned int	dwQuality;          /* compress quality 0-10,000 */
	unsigned int	dwBytesPerSecond;   /* bytes per second */
	unsigned int	dwFlags;            /* flags... see below */
	unsigned int	dwInterleaveEvery;  /* for non-video streams only */
	unsigned int	pad;

	char			avicodecname[128];
} AviCodecData;

typedef struct QuicktimeCodecData {

	void			*cdParms;   /* codec/compressor options */
	void			*pad;	    /* padding */

	unsigned int	cdSize;		    /* size of cdParms buffer */
	unsigned int	pad2;		    /* padding */

	char			qtcodecname[128];
} QuicktimeCodecData;

typedef struct FFMpegCodecData {
	int type;
	int codec;
	int audio_codec;
	int video_bitrate;
	int audio_bitrate;
	int gop_size;
	int flags;

	int rc_min_rate;
	int rc_max_rate;
	int rc_buffer_size;
	int mux_packet_size;
	int mux_rate;
} FFMpegCodecData;


typedef struct AudioData {
	int mixrate;
	float main;		/* Main mix in dB */
	short flag;
	short pad[3];
} AudioData;

typedef struct SceneRenderLayer {
	struct SceneRenderLayer *next, *prev;
	
	char name[32];
	
	struct Material *mat_override;
	struct Group *light_override;
	
	unsigned int lay;		/* scene->lay itself has priority over this */
	int layflag;
	int passflag;			/* pass_xor has to be after passflag */
	int pass_xor;
} SceneRenderLayer;

/* srl->layflag */
#define SCE_LAY_SOLID	1
#define SCE_LAY_ZTRA	2
#define SCE_LAY_HALO	4
#define SCE_LAY_EDGE	8
#define SCE_LAY_SKY		16
#define SCE_LAY_STRAND	32
	/* flags between 32 and 0x8000 are set to 1 already, for future options */

#define SCE_LAY_ALL_Z	0x8000
#define SCE_LAY_XOR		0x10000
#define SCE_LAY_DISABLE	0x20000

/* srl->passflag */
#define SCE_PASS_COMBINED	1
#define SCE_PASS_Z			2
#define SCE_PASS_RGBA		4
#define SCE_PASS_DIFFUSE	8
#define SCE_PASS_SPEC		16
#define SCE_PASS_SHADOW		32
#define SCE_PASS_AO			64
#define SCE_PASS_REFLECT	128
#define SCE_PASS_NORMAL		256
#define SCE_PASS_VECTOR		512
#define SCE_PASS_REFRACT	1024
#define SCE_PASS_INDEXOB	2048
#define SCE_PASS_UV			4096
#define SCE_PASS_RADIO		8192
#define SCE_PASS_INDEXMAT	16384
#define SCE_PASS_MIST		32768

/* note, srl->passflag is treestore element 'nr' in outliner, short still... */


typedef struct RenderData {
	
	struct AviCodecData *avicodecdata;
	struct QuicktimeCodecData *qtcodecdata;
	struct FFMpegCodecData ffcodecdata;

	int cfra, sfra, efra;	/* frames as in 'images' */
	int psfra, pefra;		/* start+end frames of preview range */

	int images, framapto;
	short flag, threads;

	float ctime;			/* use for calcutions */
	float framelen, blurfac;

	/** For UR edge rendering: give the edges this color */
	float edgeR, edgeG, edgeB;
	
	short fullscreen, xplay, yplay, freqplay;	/* standalone player */
	short depth, attrib, rt1, rt2;			/* standalone player */

	short stereomode;	/* standalone player stereo settings */
	
	short dimensionspreset;		/* for the dimensions presets menu */
 	
 	short filtertype;	/* filter is box, tent, gauss, mitch, etc */

	short size, maximsize;	/* size in %, max in Kb */
	/* from buttons: */
	/**
	 * The desired number of pixels in the x direction
	 */
	short xsch;
	/**
	 * The desired number of pixels in the y direction
	 */
	short ysch;
	/**
	 * Adjustment factors for the aspect ratio in the x direction
	 */
	short xasp;
	/**
	 * Adjustment factors for the aspect ratio in the x direction
	 */
	short yasp;
	/**
	 * The number of part to use in the x direction
	 */
	short xparts;
	/**
	 * The number of part to use in the y direction
	 */
	short yparts;
        
	short winpos, planes, imtype, subimtype;
	
	/** Mode bits:                                                           */
	/* 0: Enable backbuffering for images                                    */
	short bufflag;
 	short quality;
	
	/**
	 * Flags for render settings. Use bit-masking to access the settings.
	 */
	short scemode;

	/**
	 * Flags for render settings. Use bit-masking to access the settings.
	 */
	int mode;

	/* render engine, octree resolution */
	short renderer, ocres, rpad[2];

	/**
	 * What to do with the sky/background. Picks sky/premul/key
	 * blending for the background
	 */
	short alphamode;

	/**
	 * The number of samples to use per pixel.
	 */
	short osa;
	
	/* safety, border and display rect */
	rctf safety, border;
	rcti disprect;
	
	/* information on different layers to be rendered */
	ListBase layers;
	short actlay, pad2;

	short edgeint;
	short frs_sec;
	float frs_sec_base;
	
	/**
	 * Value used to define filter size for all filter options  */
	float gauss;
	
	/** post-production settings. Depricated, but here for upwards compat (initialized to 1) */	 
	float postmul, postgamma, posthue, postsat;	 
	
 	/* Dither noise intensity */
	float dither_intensity;
	
	/* Bake Render options */
	short bake_osa, bake_filter, bake_mode, bake_flag;
	
	/* yafray: global panel params. TODO: move elsewhere */
	short GIquality, GIcache, GImethod, GIphotons, GIdirect;
	short YF_AA, YFexportxml, YF_nobump, YF_clamprgb, yfpad1;
	int GIdepth, GIcausdepth, GIpixelspersample;
	int GIphotoncount, GImixphotons;
	float GIphotonradius;
	int YF_numprocs, YF_raydepth, YF_AApasses, YF_AAsamples;
	float GIshadowquality, GIrefinement, GIpower, GIindirpower;
	float YF_gamma, YF_exposure, YF_raybias, YF_AApixelsize, YF_AAthreshold;

	/* paths to backbufffer, output, ftype */
	char backbuf[160], pic[160], ftype[160];

	/* stamp flag. */
	int stamp;

	/* stamp info. */
	char stamp_u_data[160];

	/* stamp color. */
	float cstamp[4];
	float stamppad;

	/* cineon */
	short cineonwhite, cineonblack;
	float cineongamma;
} RenderData;


typedef struct GameFraming {
	float col[3];
	char type, pad1, pad2, pad3;
} GameFraming;

#define SCE_GAMEFRAMING_BARS   0
#define SCE_GAMEFRAMING_EXTEND 1
#define SCE_GAMEFRAMING_SCALE  2

typedef struct TimeMarker {
	struct TimeMarker *next, *prev;
	int frame;
	char name[64];
	unsigned int flag;
} TimeMarker;

struct ImagePaintSettings {
	struct Brush *brush;
	short flag, tool;
	int pad3;
};

typedef struct ToolSettings {
	/* Subdivide Settings */
	short cornertype;
	short editbutflag;
	/*Triangle to Quad conversion threshold*/
	float jointrilimit;
	/* Extrude Tools */
	float degr; 
	short step;
	short turn; 
	
	float extr_offs; 
	float doublimit;
	
	/* Primitive Settings */
	/* UV Sphere */
	short segments;
	short rings;
	
	/* Cylinder - Tube - Circle */
	short vertices;

	/* UV Calculation */
	short unwrapper;
	float uvcalc_radius;
	float uvcalc_cubesize;
	short uvcalc_mapdir;
	short uvcalc_mapalign;
	short uvcalc_flag;

	short pad2;
	
	/* Image Paint (8 byte aligned please!) */
	struct ImagePaintSettings imapaint;
	
	/* Select Group Threshold */
	float select_thresh;
	
	/* IPO-Editor */
	float clean_thresh;
	
	/* Retopo */
	char retopo_mode;
	char retopo_paint_tool;
	char line_div, ellipse_div, retopo_hotspot;

	/* Multires */
	char multires_subdiv_type;
	
	char  pad4[2];
} ToolSettings;

/* Used by all brushes to store their properties, which can be directly set
   by the interface code. Note that not all properties are actually used by
   all the brushes. */
typedef struct BrushData
{
	short size;
	char strength, dir; /* Not used for smooth brush */
	char airbrush;
	char view;
	char pad[2];
} BrushData;

struct SculptSession;
typedef struct SculptData
{
	/* Data stored only from entering sculptmode until exiting sculptmode */
	struct SculptSession *session;

	/* Pointers to all of sculptmodes's textures */
	struct MTex *mtex[10];

	/* Settings for each brush */
	BrushData drawbrush, smoothbrush, pinchbrush, inflatebrush, grabbrush, layerbrush, flattenbrush;
	short brush_type;

	/* For the Brush Shape */
	short texact, texnr;
	short spacing;
	char texrept;
	char texfade;
	char texsep;

	char averaging;
	
	char draw_flag;
	
	/* Control tablet input */
	char tablet_size, tablet_strength;
	
	/* Symmetry is separate from the other BrushData because the same
	   settings are always used for all brush types */
	char symm;
} SculptData;

typedef struct ScenePlumi {
	struct ScenePlumi *next, *prev;
	struct Scene *scene;
} ScenePlumi;

typedef struct Scene {
	ID id;
	struct Object *camera;
	struct World *world;
	
	struct Scene *set;
	struct Image *ima;
	
	ListBase base;
	struct Base *basact;
	
	float cursor[3];
	float twcent[3];			/* center for transform widget */
	float twmin[3], twmax[3];	/* boundbox of selection for transform widget */
	unsigned int lay;
	
	/* editmode stuff */
	float editbutsize;                      /* size of normals */
	short selectmode;
	short proportional, prop_mode;
	
	short use_nodes;
	struct bNodeTree *nodetree;	
	
	void *ed;
	struct Radio *radio;
	void *sumohandle;
	
	struct GameFraming framing;

	struct ToolSettings *toolsettings;

	/* migrate or replace? depends on some internal things... */
	/* no, is on the right place (ton) */
	struct RenderData r;
	struct AudioData audio;	
	
	ScriptLink scriptlink;
	
	ListBase markers;
	
	short jumpframe;
	short pad1, pad2, pad3;
	
	/* none of the dependancy graph  vars is mean to be saved */
	struct  DagForest *theDag;
	short dagisvalid, dagflags;
	short pad4, recalc;				/* recalc = counterpart of ob->recalc */

	/* Sculptmode data */
	struct SculptData sculptdata;

	/* Scene list to include render layers (background scene). */
	ListBase bg_scenes;
	short bg_flags;
	short pad5;

	/* frame step, to render. */
	int frame_step;

	/* Scene list to include in compo. */
	ListBase compo_scenes;
	short compo_flags;
	short pad6;
	int pad7;
} Scene;


/* **************** RENDERDATA ********************* */

/* bufflag */
#define R_BACKBUF		1
#define R_BACKBUFANIM	2
#define R_FRONTBUF		4
#define R_FRONTBUFANIM	8

/* mode (int now) */
#define R_OSA			0x0001
#define R_SHADOW		0x0002
#define R_GAMMA			0x0004
#define R_ORTHO			0x0008
#define R_ENVMAP		0x0010
#define R_EDGE			0x0020
#define R_FIELDS		0x0040
#define R_FIELDSTILL	0x0080
#define R_RADIO			0x0100
#define R_BORDER		0x0200
#define R_PANORAMA		0x0400
#define R_CROP			0x0800
#define R_COSMO			0x1000
#define R_ODDFIELD		0x2000
#define R_MBLUR			0x4000
		/* unified was here */
#define R_RAYTRACE      0x10000
		/* R_GAUSS is obsolete, but used to retrieve setting from old files */
#define R_GAUSS      	0x20000
		/* fbuf obsolete... */
#define R_FBUF			0x40000
		/* threads obsolete... is there for old files */
#define R_THREADS		0x80000
#define R_SPEED			0x100000
#define R_SSS			0x200000

/* filtertype */
#define R_FILTER_BOX	0
#define R_FILTER_TENT	1
#define R_FILTER_QUAD	2
#define R_FILTER_CUBIC	3
#define R_FILTER_CATROM	4
#define R_FILTER_GAUSS	5
#define R_FILTER_MITCH	6
#define R_FILTER_FAST_GAUSS	7 /* note, this is only used for nodes at the moment */

/* yafray: renderer flag (not only exclusive to yafray) */
#define R_INTERN	0
#define R_YAFRAY	1

/* scemode */
#define R_DOSEQ				0x0001
#define R_BG_RENDER			0x0002
		/* passepartout is camera option now, keep this for backward compatibility */
#define R_PASSEPARTOUT		0x0004
#define R_PREVIEWBUTS		0x0008
#define R_EXTENSION			0x0010
#define R_NODE_PREVIEW		0x0020
#define R_DOCOMP			0x0040
#define R_COMP_CROP			0x0080
#define R_FREE_IMAGE		0x0100
#define R_SINGLE_LAYER		0x0200
#define R_EXR_TILE_FILE		0x0400
#define R_COMP_FREE			0x0800
#define R_NO_IMAGE_LOAD		0x1000
#define R_NO_TEX		0x2000
#define R_STAMP_INFO 0x4000

/* r->stamp flag */
#define R_STAMP_TIME 0x0001
#define R_STAMP_FRAME 0x0002
#define R_STAMP_DATE 0x0004
#define R_STAMP_CAMERA 0x0008
#define R_STAMP_SCENE 0x0010
#define R_STAMP_USER 0x0020

/* alphamode */
#define R_ADDSKY		0
#define R_ALPHAPREMUL	1
#define R_ALPHAKEY		2

/* planes */
#define R_PLANES24		24
#define R_PLANES32		32
#define R_PLANESBW		8

/* imtype */
#define R_TARGA		0
#define R_IRIS		1
#define R_HAMX		2
#define R_FTYPE		3
#define R_JPEG90	4
#define R_MOVIE		5
#define R_IRIZ		7
#define R_RAWTGA	14
#define R_AVIRAW	15
#define R_AVIJPEG	16
#define R_PNG		17
#define R_AVICODEC	18
#define R_QUICKTIME 19
#define R_BMP		20
#define R_RADHDR	21
#define R_TIFF		22
#define R_OPENEXR	23
#define R_FFMPEG        24
#define R_FRAMESERVER   25
#define R_CINEON		26
#define R_DPX			27
#define R_MULTILAYER	28

/* subimtype, flag options for imtype */
#define R_OPENEXR_HALF	1
#define R_OPENEXR_ZBUF	2
#define R_PREVIEW_JPG	4
#define R_CINEON_LOG 	8
#define R_TIFF_16BIT	16

/* bake_mode: same as RE_BAKE_xxx defines */
/* bake_flag: */
#define R_BAKE_CLEAR	1
#define R_BAKE_OSA		2

/* **************** SCENE ********************* */
#define RAD_PHASE_PATCHES	1
#define RAD_PHASE_FACES		2

/* base->flag is in DNA_object_types.h */

/* sce->flag */
#define SCE_ADDSCENAME		1

/* sce->selectmode */
#define SCE_SELECT_VERTEX	1
#define SCE_SELECT_EDGE		2
#define SCE_SELECT_FACE		4

/* sce->bg_flags / sce->compo_flags */
#define SCE_USE_BACKGROUND 1
#define SCE_USE_COMPO 1

/* sce->recalc (now in use by previewrender) */
#define SCE_PRV_CHANGED		1

/* sce->prop_mode (proportional falloff) */
#define PROP_SMOOTH            0
#define PROP_SPHERE            1
#define PROP_ROOT              2
#define PROP_SHARP             3
#define PROP_LIN               4
#define PROP_CONST             5
#define PROP_RANDOM		6

	/* return flag next_object function */
#define F_START			0
#define F_SCENE			1
#define F_SET			2
#define F_DUPLI			3

/* audio->flag */
#define AUDIO_MUTE		1
#define AUDIO_SYNC		2
#define AUDIO_SCRUB		4

#define FFMPEG_MULTIPLEX_AUDIO  1
#define FFMPEG_AUTOSPLIT_OUTPUT 2

/* SculptData.brushtype */
#define DRAW_BRUSH 1
#define SMOOTH_BRUSH 2
#define PINCH_BRUSH 3
#define INFLATE_BRUSH 4
#define GRAB_BRUSH 5
#define LAYER_BRUSH 6
#define FLATTEN_BRUSH 7
/* SculptData.texrept */
#define SCULPTREPT_DRAG 1
#define SCULPTREPT_TILE 2
#define SCULPTREPT_3D   3
/* SculptData.draw_flag */
#define SCULPTDRAW_FAST  1
#define SCULPTDRAW_BRUSH 2

#define SYMM_X 1
#define SYMM_Y 2
#define SYMM_Z 4

/* toolsettings->imagepaint_flag */
#define IMAGEPAINT_DRAWING				1
#define IMAGEPAINT_DRAW_TOOL			2
#define IMAGEPAINT_DRAW_TOOL_DRAWING	4

/* toolsettings->retopo_mode */
#define RETOPO 1
#define RETOPO_PAINT 2

/* toolsettings->retopo_paint_tool */
#define RETOPO_PEN 1
#define RETOPO_LINE 2
#define RETOPO_ELLIPSE 4

#ifdef __cplusplus
}
#endif

#endif

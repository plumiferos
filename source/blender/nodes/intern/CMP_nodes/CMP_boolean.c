/*
 * Signed-off-by: Diego Hernan Borghetti <bdiego@gmail.com>
 */

#include "../CMP_util.h"


/* ************** Boolean ************** */
static bNodeSocketType cmp_node_boolean_in[]= {
	{ SOCK_VALUE, 1, "Limit", 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f },
	{ SOCK_RGBA, 1, "A", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f },
	{ SOCK_RGBA, 1, "B", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static bNodeSocketType cmp_node_boolean_out[]= {
	{ SOCK_RGBA, 0, "Image", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static void do_boolean(bNode *node, float *out, float *limit, float *in1, float *in2)
{
	boolean_operation(node->custom1, out, *limit, in1, in2);
}

static void node_composit_exec_boolean(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	if(out[0]->hasoutput) {
		if((!in[1]->data) && (!in[2]->data)) {
			/* input no image? then only color operation. */
			do_boolean(node, out[0]->vec, in[0]->vec, in[1]->vec, in[2]->vec);
		}
		else {
			int stackbufheight, stackbufwidth, stackbuftype;
			CompBuf *limit= in[0]->data;
			CompBuf *cbuf1= in[1]->data;
			CompBuf *cbuf2= in[2]->data;
			CompBuf *stackbuf;

			/* set stackbuftype */
			if ((!cbuf1) && (!cbuf2))
				stackbuftype= CB_VAL;
			else
				stackbuftype= CB_RGBA;

			/* make output size the smallest possible */
			if(!cbuf1) {
				stackbufheight= cbuf2->y;
				stackbufwidth= cbuf2->x;
			}
			else if(!cbuf2) {
				stackbufheight= cbuf1->y;
				stackbufwidth= cbuf1->x;
			}
			else {
				stackbufheight= MIN2(cbuf1->y, cbuf2->y);
				stackbufwidth= MIN2(cbuf1->x, cbuf2->x);
			}

			stackbuf= alloc_compbuf(stackbufwidth, stackbufheight, stackbuftype, 1);
			composit3_pixel_processor(node, stackbuf, limit, in[0]->vec, cbuf1, in[1]->vec, cbuf2, in[2]->vec, do_boolean, CB_VAL, CB_RGBA, CB_RGBA);
			out[0]->data= stackbuf;
		}
	}
}

bNodeType cmp_node_boolean= {
	/* next, prev  */ NULL, NULL,
	/* type code   */ CMP_NODE_BOOLEAN,
	/* name        */ "Boolean",
	/* width+range */ 80, 40, 120,
	/* class+opts  */ NODE_CLASS_CONVERTOR, NODE_OPTIONS,
	/* input sock  */ cmp_node_boolean_in,
	/* output sock */ cmp_node_boolean_out,
	/* storage     */ "",
	/* execfunc    */ node_composit_exec_boolean,
	/* butsfunc    */ NULL,
	/* initfunc    */ NULL,
	/* freestoragefunc */ NULL,
	/* copystoragefunc */ NULL,
	/* id          */ NULL
};

/*
 * write by: Diego Hernan Borghetti.
 */

#include "../CMP_util.h"


/* **************** Empty ******************* */
static bNodeSocketType cmp_node_empty_in[]= {
	{ SOCK_RGBA, 1, "Image", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static bNodeSocketType cmp_node_empty_out[]= {
	{ SOCK_RGBA, 0, "Image", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static void node_composit_exec_empty(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	CompBuf *cbuf, *stackbuf;

	if(in[0]->data) {
		cbuf= in[0]->data;
		stackbuf= pass_on_compbuf(cbuf);
		out[0]->data= stackbuf;
	}
}

bNodeType cmp_node_empty= {
	/* *next, *prev    */ NULL, NULL,
	/* type code       */ CMP_NODE_EMPTY,
	/* name            */ "Empty",
	/* width+range     */ 120, 100, 280,
	/* class+opts      */ NODE_CLASS_INPUT, NODE_OPTIONS,
	/* input sock      */ cmp_node_empty_in,
	/* output sock     */ cmp_node_empty_out,
	/* storage         */ "",
	/* execfunc        */ node_composit_exec_empty,
	/* butfunc         */ NULL,
	/* initfunc        */ NULL,
	/* freestoragefunc */ NULL,
	/* copystoragefunc */ NULL,
	/* id              */ NULL
};


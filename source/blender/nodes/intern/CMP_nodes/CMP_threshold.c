/**
 *
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * The Original Code is Copyright (C) 2006 Blender Foundation.
 * All rights reserved.
 *
 * The Original Code is: all of this file.
 *
 * Contributor(s): Franasois Grassard (coyhot)
 *
 * ***** END GPL LICENSE BLOCK *****
 */

#include "../CMP_util.h"


/* ************** Threshold ************** */
static bNodeSocketType cmp_node_threshold_in[]= {
	{ SOCK_RGBA, 1, "Image", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f},
	{ SOCK_VALUE, 1, "Threshold", 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f},
	{ SOCK_RGBA, 1, "Under", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
	{ SOCK_RGBA, 1, "Over", 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f},
	{ -1, 0, "" }
};

static bNodeSocketType cmp_node_threshold_out[]= {
	{ SOCK_RGBA, 0, "Image", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static void node_composit_exec_threshold(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	int i, src_pix, src_width, src_height, src_ydelt, x, y;
	float *srcpix, *outpix, *rememberpix;
	float threshold = in[1]->vec[0];
	float bw = 0, filterpixel = 0;

	if (!in[0]->data)
		return;

	CompBuf *cbuf= in[0]->data;
	CompBuf *stackbuf= alloc_compbuf(cbuf->x, cbuf->y, CB_RGBA, 1);

	src_pix= cbuf->type;
	src_width= cbuf->x;
	src_height= cbuf->y;
	src_ydelt= src_width*src_pix;

	srcpix= cbuf->rect;
	outpix= stackbuf->rect;

	/* ByPass On */
	if(node->custom1) {
		for(y=0; y<src_height; y++) {
			for(x=0; x<src_width; x++) {
				for(i=0; i<src_pix; i++) {
					outpix[i] = srcpix[i];
				}
				outpix += src_pix;
				srcpix += src_pix;
			}
		}
		out[0]->data= stackbuf;
		return;
	}

	/* ByPass Off */
	for(y=0; y<src_height; y++) {
		for(x=0; x<src_width; x++) {
			if(node->custom2) {
				/* With filter on */
				if(x<src_width-1 && y<src_height-1) {
					rememberpix = srcpix;
					bw = (srcpix[0]+srcpix[1]+srcpix[2])/3;

					if (bw > threshold)
						bw = 1.0;
					else
						bw = 0.0;

					filterpixel += bw;
					srcpix += src_pix;
					bw = (srcpix[0]+srcpix[1]+srcpix[2])/3;

					if (bw > threshold)
						bw = 1.0;
					else
						bw = 0.0;

					filterpixel += bw;
					srcpix += src_ydelt;
					bw = (srcpix[0]+srcpix[1]+srcpix[2])/3;

					if (bw > threshold)
						bw = 1.0;
					else
						bw = 0.0;

					filterpixel += bw;
					srcpix -= src_pix;
					bw = (srcpix[0]+srcpix[1]+srcpix[2])/3;

					if (bw > threshold)
						bw = 1.0;
					else
						bw = 0.0;

					filterpixel += bw;
					filterpixel /= 4;
					srcpix = rememberpix;

					for(i=0; i<src_pix; i++) {
						outpix[i] = (in[2]->vec[i]*(1-filterpixel)+in[3]->vec[i]*filterpixel);
					}
				}
			}
			else
			{
				bw = (srcpix[0]+srcpix[1]+srcpix[2])/3;
				if (bw > threshold)
					bw = 1.0;
				else
					bw = 0.0;

				filterpixel = bw;
				for(i=0; i<src_pix; i++) {
					outpix[i] = (in[2]->vec[i]*(1-filterpixel)+in[3]->vec[i]*filterpixel);
				}
			}
			outpix += src_pix;
			srcpix += src_pix;
		}
	}
	out[0]->data= stackbuf;
}

bNodeType cmp_node_threshold= {
	/* next, prev   */ NULL, NULL,
	/* type code    */ CMP_NODE_THRESHOLD,
	/* name         */ "Threshold",
	/* width+range  */ 140, 100, 320,
	/* class+opts   */ NODE_CLASS_OP_FILTER, NODE_OPTIONS,
	/* input sock   */ cmp_node_threshold_in,
	/* output sock  */ cmp_node_threshold_out,
	/* storage      */ "",
	/* execfunc     */ node_composit_exec_threshold,
	/* butfunc      */ NULL,
	/* initfunc     */ NULL,
	/* freenodestorage */ NULL,
	/* copynodestorage */ NULL,
	/* id           */ NULL
};

/**
 *
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * The Original Code is Copyright (C) 2006 Blender Foundation.
 * All rights reserved.
 *
 * The Original Code is: all of this file.
 *
 * Contributor(s): Alfredo de Greef  (eeshlo)
 *
 * ***** END GPL LICENSE BLOCK *****
 */

#include "../CMP_util.h"

static bNodeSocketType cmp_node_fgblur_in[]= {
	{	SOCK_RGBA, 1, "Image",			0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f},
	{	-1, 0, ""	}
};
static bNodeSocketType cmp_node_fgblur_out[]= {
	{	SOCK_RGBA, 0, "Image",			0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
	{	-1, 0, ""	}
};

static void node_composit_exec_fgblur(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	NodeFGBlurData *nbd = node->storage;
	CompBuf *new, *img = in[0]->data;
	float sx, sy;
	int c;

	if ((img==NULL) || (out[0]->hasoutput==0)) return;

	if (img->type == CB_VEC2)
		new = typecheck_compbuf(img, CB_VAL);
	else if (img->type == CB_VEC3)
		new = typecheck_compbuf(img, CB_RGBA);
	else
		new = dupalloc_compbuf(img);

	nbd->imageiw= img->x;
	nbd->imageih= img->y;

	if (nbd->relative) {
		sx= (float) nbd->percentx * img->x;
		sy= (float) nbd->percenty * img->y;
	}
	else {
		sx= nbd->sizex;
		sy= nbd->sizey;
	}

	if ((sx == sy) && (sx > 0.f)) {
		for (c=0; c<new->type; ++c)
			IIR_gauss(new, sx, c, 3);
	}
	else {
		if (sx > 0.f) {
			for (c=0; c<new->type; ++c)
				IIR_gauss(new, sx, c, 1);
		}
		if (sy > 0.f) {
			for (c=0; c<new->type; ++c)
				IIR_gauss(new, sy, c, 2);
		}
	}

	out[0]->data = new;
}

static void node_composit_init_fgblur(bNode *node)
{
	node->storage= MEM_callocN(sizeof(NodeFGBlurData), "node fg blur data");
}

bNodeType cmp_node_fgblur = {
	/* *next,*prev */	NULL, NULL,
	/* type code   */	CMP_NODE_FGBLUR,
	/* name        */	"Fast Gauss.Blur",
	/* width+range */	150, 120, 200,
	/* class+opts  */	NODE_CLASS_OP_FILTER, NODE_OPTIONS,
	/* input sock  */	cmp_node_fgblur_in,
	/* output sock */	cmp_node_fgblur_out,
	/* storage     */	"NodeFGBlurData",
	/* execfunc    */	node_composit_exec_fgblur,
	/* butfunc     */	NULL,
	/* initfunc    */	node_composit_init_fgblur,
	/* freestoragefunc    */	node_free_standard_storage,
	/* copystoragefunc    */	node_copy_standard_storage,
	/* id          */	NULL
};

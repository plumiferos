/*
 * Signed-off-by: Diego Hernan Borghetti <bdiego@gmail.com>
 */

#include "../CMP_util.h"


/* ************** Radial Blur ************** */
static bNodeSocketType cmp_node_radialblur_in[]= {
	{ SOCK_RGBA, 1, "Image", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f },
	{ SOCK_VALUE, 1, "Factor", 1, 0, 0, 0, 0, 500 },
	{ SOCK_VALUE, 1, "Center X", 50.0, 0.0, 0.0, 0.0, -500, 500 },
	{ SOCK_VALUE, 1, "Center Y", 50.0, 0.0, 0.0, 0.0, -500, 500 },
	{ -1, 0, "" }
};

static bNodeSocketType cmp_node_radialblur_out[]= {
	{ SOCK_RGBA, 0, "Image", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static void node_composit_exec_radialblur(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	if(in[0]->data) {
		CompBuf *cbuf= in[0]->data;
		CompBuf *stackbuf= alloc_compbuf(cbuf->x, cbuf->y, cbuf->type, 1);
		int i, j, src_pix, src_width, src_height, srcydelt, x, y;
		int dirx, diry, blur_center_x, blur_center_y, step;
		float sumR= 0.0f, sumG= 0.0f, sumB= 0.0, blur_factor;
		double angle, radius, normalize_radius, max_radius;
		float *srcpix, *srcdestpix, *outpix;

		src_pix= cbuf->type;
		src_width= cbuf->x;
		src_height= cbuf->y;
		srcpix= cbuf->rect;
		outpix= stackbuf->rect;
		srcdestpix= outpix;
		srcydelt= src_width*src_pix;

		step= (int)node->custom1;
		if(step<1)
			step= 1;

		/* Bypass off */
		blur_factor= (int)(in[1]->vec[0]);
		blur_center_x= (int)(in[2]->vec[0]);
		blur_center_y= (int)(in[3]->vec[0]);
		max_radius= sqrt(src_width*src_width + src_height*src_height);
		blur_factor= (max_radius*blur_factor)/100;
		blur_center_x= (src_width*blur_center_x)/100;
		blur_center_y= (src_height*blur_center_y)/100;

		for(y= 0; y<src_height; y++) {
			for(x= 0; x<src_width; x++) {
				dirx= blur_center_x-x;
				diry= blur_center_y-y;
				radius= sqrt(dirx*dirx + diry*diry);
				angle= atan2(diry, dirx);
				normalize_radius= (radius*blur_factor)/max_radius;

				for(j= 0; j<normalize_radius; j+=step) {
					srcpix= cbuf->rect;
					dirx= (int)(blur_center_x-(radius-j) * cos(angle));
					diry= (int)(blur_center_y-(radius-j) * sin(angle));

					dirx= (dirx<0) ? 0 : ((dirx>=src_width) ? src_width-1 : dirx);
					diry= (diry<0) ? 0 : ((diry>=src_height) ? src_height-1 : diry);
					srcpix+= diry*srcydelt + dirx*src_pix;
					sumR+= srcpix[0];
					sumG+= srcpix[1];
					sumB+= srcpix[2];
				}

				outpix= stackbuf->rect;
				outpix+= y*srcydelt + x*src_pix;
				if(normalize_radius) {
					outpix[0]= sumR/(int)((normalize_radius/step)+1);
					outpix[1]= sumG/(int)((normalize_radius/step)+1);
					outpix[2]= sumB/(int)((normalize_radius/step)+1);
				}
				else {
					outpix[0]= srcpix[0];
					outpix[1]= srcpix[1];
					outpix[2]= srcpix[2];
				}
				sumR= sumG= sumB= 0;
			}
		}
		out[0]->data= stackbuf;
	}
}

bNodeType cmp_node_radialblur= {
	/* next, prev   */ NULL, NULL,
	/* type code    */ CMP_NODE_RADIAL_BLUR,
	/* name         */ "Radial Blur",
	/* width+range  */ 140, 100, 320,
	/* class+opts   */ NODE_CLASS_OP_FILTER, NODE_OPTIONS,
	/* input sock   */ cmp_node_radialblur_in,
	/* output sock  */ cmp_node_radialblur_out,
	/* storage      */ "",
	/* execfunc     */ node_composit_exec_radialblur,
	/* butsfunc     */ NULL,
	/* initfunc     */ NULL,
	/* freestoragefunc */ NULL,
	/* copystoragefunc */ NULL,
	/* id           */ NULL
};

/*
 * write by: Diego Hernan Borghetti.
 */

#include "../CMP_util.h"

/* ******************* Fix ********************* */
static bNodeSocketType cmp_node_fix_in[]= {
	{ SOCK_RGBA, 1, "Image", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static bNodeSocketType cmp_node_fix_out[]= {
	{ SOCK_RGBA, 0, "Image", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static void do_fix_pixel(CompBuf *ibuf)
{
	float *fp, *fps;
	int tot, x, fixr, fixg, fixb, need_fix;

	tot= ibuf->x * ibuf->y;
	fp= ibuf->rect;
	fixr= 0;
	fixg= 0;
	fixb= 0;

	for (x= tot; x>0; x--, fp+= 4) {
		need_fix= 0;

		if((fp[0] > 2.0) || (fp[0] < -2.0)) {
			fixr++;
			need_fix++;
		}
		if((fp[1] > 2.0) || (fp[1] < -2.0)) {
			fixg++;
			need_fix++;
		}
		if((fp[2] > 2.0) || (fp[2] < -2.0)) {
			fixb++;
			need_fix++;
		}

		if(need_fix) {
			if(x != tot)
				fps= fp-4;
			else
				fps= fp+4;

			fp[0]= fps[0];
			fp[1]= fps[1];
			fp[2]= fps[2];
		}
	}

	if((fixr) || (fixg) || (fixb))
		printf("Fixed (%d) red, (%d) green, (%d) blue pixels.\n", fixr, fixg, fixb);
}
				
static void node_composit_exec_fix(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	CompBuf *cbuf, *stackbuf;

	if(in[0]->data) {
		cbuf= in[0]->data;
		stackbuf= pass_on_compbuf(cbuf);
		do_fix_pixel(stackbuf);
		out[0]->data= stackbuf;
	}
}

bNodeType cmp_node_fix= {
	/* *next, *prev	 */ NULL, NULL,
	/* type code		*/ CMP_NODE_FIX,
	/* name			 */ "Fix",
	/* width+range	  */ 120, 100, 280,
	/* class+opts	   */ NODE_CLASS_INPUT, NODE_OPTIONS,
	/* input sock	   */ cmp_node_fix_in,
	/* output sock	  */ cmp_node_fix_out,
	/* storage		  */ "",
	/* execfunc		 */ node_composit_exec_fix,
	/* butfunc		  */ NULL,
	/* initfunc		 */ NULL,
	/* freestoragefunc  */ NULL,
	/* copystoragefunc  */ NULL,
	/* id			   */ NULL
};

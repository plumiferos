/**
* $Id: CMP_normal_map.c 10516 2007-04-13 04:22:32Z scourage $
*
* ***** BEGIN GPL LICENSE BLOCK *****
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version. 
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software Foundation,
* Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
* 
* The Original Code is Copyright (C) 2006 Blender Foundation.
* All rights reserved.
* 
* The Original Code is: all of this file.
* 
* Contributor(s): none yet.
* 
* ***** END GPL LICENSE BLOCK *****

*/

#include "../CMP_util.h"


/* **************** Normal Tools  ******************** */
  
static bNodeSocketType cmp_node_normal_in[]= {
	{	SOCK_RGBA, 1, "Image",		0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
	{   SOCK_VALUE, 1, "Index n1",             0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f},
	{   SOCK_VALUE, 1, "Index n2",             0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f},
    {	-1, 0, ""	}
};
static bNodeSocketType cmp_node_normal_out[]= {
	{	SOCK_RGBA, 0, "Image",			0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
	{	SOCK_VECTOR, 0, "Displacement",			0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
	{	-1, 0, ""	}
};

static void do_normal_map(CompBuf *dispbuf,CompBuf *stackbuf, CompBuf *cbuf,float *vec,float n1,float n2)
{
	int x, y, sx, sy;
	float *out= stackbuf->rect;
	float *disp=dispbuf->rect;
	float *xright,*xleft,*height;
	float *ytop,*ybot;
	float sum;
	float anglex,angley,indexratio;
	int dxp,dyp,dxn,dyn;

	
	sx=cbuf->x;
	sy=cbuf->y;
	
		for (y=0; y < sy; y++) {
			for (x=0; x < sx; x++,out+=stackbuf->type,disp+=dispbuf->type) {

				height = compbuf_get_pixel(cbuf, vec, x-cbuf->xrad, y-cbuf->yrad, cbuf->xrad, cbuf->yrad);

				//find the tangent vectors in x and y directions
				//check corners and sides
				if (x == 0) dxn = 0;
				else dxn = -1;
				if (y == 0) dyn = 0;
				else dyn = -1;
				if (x == sx-1) dxp = 0;
				else dxp = 1;
				if (y == sy-1) dyp = 0;
				else dyp = 1;

					xright = compbuf_get_pixel(cbuf, vec,x+dxp-cbuf->xrad,y-cbuf->yrad, cbuf->xrad, cbuf->yrad);
					xleft = compbuf_get_pixel(cbuf, vec,x+dxn-cbuf->xrad,y-cbuf->yrad, cbuf->xrad, cbuf->yrad);
					ytop = compbuf_get_pixel(cbuf, vec,x-cbuf->xrad,y+dyp-cbuf->yrad, cbuf->xrad, cbuf->yrad);
					ybot = compbuf_get_pixel(cbuf, vec,x-cbuf->xrad,y+dyn-cbuf->yrad, cbuf->xrad, cbuf->yrad);
				
	
				//calculate the difference in height
				out[0]=(xright[0]+xright[1]+xright[2])- (xleft[0]+xleft[1]+xleft[2]);
				out[1]=(ytop[0]+ytop[1]+ytop[2])- (ybot[0]+ybot[1]+ybot[2]);
					
				
				sum = sqrt(out[0]*out[0]+out[1]*out[1]+1);

				//angle between the normal and a vertical vector
				indexratio = n1/n2;
				anglex = acos((1/sum)/(sqrt((out[0]/sum)*(out[0]/sum)+ 1/(sum*sum))));
				angley = acos((1/sum)/(sqrt((out[1]/sum)*(out[1]/sum)+ 1/(sum*sum))));

				//calculate x and y displacement for refraction, due to the normal
				disp[0] = (3-(height[0]+height[1]+height[2]))*tan(asin((indexratio)*sin(anglex))- anglex);
				disp[1] = (3-(height[0]+height[1]+height[2]))*tan(asin((indexratio)*sin(angley))- angley);
						
				//convert to RGB, scale RG to (-1,1), B to (0,1) to get normal map
				out[0] = ((-out[0]/sum)+1)/2;
				out[1] = ((-out[1]/sum)+1)/2;
				out[2] = 1/sum;
				out[3] = 1;

			}
		}
		
}
static void node_composit_exec_normal(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	if(in[0]->data) 
	{		
		/* make output size of input image */
		CompBuf *cbuf= in[0]->data;
		CompBuf *stackbuf;
		CompBuf *dispbuf;
			
		cbuf= typecheck_compbuf(cbuf, CB_RGBA);

		// allocs
		dispbuf = alloc_compbuf(cbuf->x, cbuf->y, CB_VEC3, 1);
		stackbuf= alloc_compbuf(cbuf->x, cbuf->y, CB_RGBA, 1);
	
		do_normal_map(dispbuf,stackbuf,cbuf,in[0]->vec,in[1]->vec[0],in[2]->vec[0]);
		
		out[0]->data= stackbuf;
		out[1]->data= dispbuf;
		
		if(cbuf!=in[0]->data)
			free_compbuf(cbuf);
	}
}

bNodeType cmp_node_normal_map= {
	/* *next,*prev */	NULL, NULL,
	/* type code   */	CMP_NODE_NORMAL_MAP,
	/* name        */	"Normal Map",
	/* width+range */	140, 100, 320,
	/* class+opts  */	NODE_CLASS_OP_FILTER, NODE_OPTIONS,
	/* input sock  */	cmp_node_normal_in,
	/* output sock */	cmp_node_normal_out,
	/* storage     */	"",
	/* execfunc    */	node_composit_exec_normal,	
        /* butfunc     */       NULL,
        /* initfunc    */       NULL,
        /* freestoragefunc      */ NULL,
        /* copysotragefunc      */ NULL,
        /* id          */       NULL
};



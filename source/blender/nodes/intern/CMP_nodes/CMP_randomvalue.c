/*
 * Signed-off-by: Diego Hernan Borghetti <bdiego@gmail.com>
 */

#include "../CMP_util.h"
#include <time.h>

/* ************** Random Value ************** */
static bNodeSocketType cmp_node_random_value_in[]= {
	{ SOCK_VALUE, 1, "Min : ", 0.0, 0.0, 0.0, 0.0, -999999, 999999 },
	{ SOCK_VALUE, 1, "Max : ", 1.0, 0.0, 0.0, 0.0, -999999, 999999 },
	{ -1, 0, "" }
};

static bNodeSocketType cmp_node_random_value_out[]= {
	{ SOCK_VALUE, 0, "Value", 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static void node_composit_exec_random_value(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	float random_number, min, max, range;

	min= (float)(in[0]->vec[0]);
	max= (float)(in[1]->vec[0]);
	range= abs(max-min);
	if(range<1)
		range= 1;

	srand(time(NULL) * G.scene->r.cfra);
	random_number= min+((float)rand()*range/RAND_MAX);
	out[0]->vec[0]= random_number;
}

bNodeType cmp_node_random_value= {
	/* next, prev  */ NULL, NULL,
	/* type code   */ CMP_NODE_RANDOM_VALUE,
	/* name        */ "Random Value",
	/* width+range */ 160, 40, 200,
	/* class+opts  */ NODE_CLASS_INPUT, NODE_OPTIONS,
	/* input sock  */ cmp_node_random_value_in,
	/* output sock */ cmp_node_random_value_out,
	/* storage     */ "",
	/* execfunc    */ node_composit_exec_random_value,
	/* butsfunc    */ NULL,
	/* initfunc    */ NULL,
	/* freestoragefunc */ NULL,
	/* copystoragefunc */ NULL,
	/* id          */ NULL
};

/*
 * Signed-off-by: Diego Hernan Borghetti <bdiego@gmail.com>
 */

#include "../CMP_util.h"


/* ************** Gain ************** */
static bNodeSocketType cmp_node_gain_in[]= {
	{ SOCK_RGBA, 1, "Image", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f },
	{ SOCK_VALUE, 1, "Gain", 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 2.0f },
	{ SOCK_VALUE, 1, "Gain Red", 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 2.0f },
	{ SOCK_VALUE, 1, "Gain Green", 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 2.0f },
	{ SOCK_VALUE, 1, "Gain Blue", 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 2.0f },
	{ -1, 0, "" }
};

static bNodeSocketType cmp_node_gain_out[]= {
	{ SOCK_RGBA, 0, "Image", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static void do_gain(bNode *node, float *out, float *in, float *fac)
{
	int i;

	for(i= 0; i<3; i++) {
		out[i]= in[i]*fac[0];
		out[i]= (out[i]>1.0f) ? 1.0f : out[i];
		out[i]= (out[i]<0.0f) ? 0.0f : out[i];
	}

	out[3]= in[3];
}

static void do_gain_red(bNode *node, float *out, float *in, float *fac)
{
	out[0]= in[0]*fac[0];
	out[0]= (out[0]>1.0f) ? 1.0f : out[0];
	out[0]= (out[0]<0.0f) ? 0.0f : out[0];
}

static void do_gain_green(bNode *node, float *out, float *in, float *fac)
{
	out[1]= in[1]*fac[0];
	out[1]= (out[1]>1.0f) ? 1.0f : out[1];
	out[1]= (out[1]<0.0f) ? 0.0f : out[1];
}

static void do_gain_blue(bNode *node, float *out, float *in, float *fac)
{
	out[2]= in[2]*fac[0];
	out[2]= (out[2]>1.0f) ? 1.0f : out[2];
	out[2]= (out[2]<0.0f) ? 0.0f : out[2];
}

static void node_composit_exec_gain(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	/* stack order in: Fac, Image */
	/* stack order out: Image */
	if(!out[0]->hasoutput)
		return;

	/* input no image? then only color operation */
	if(!in[0]->data)
		do_gain(node, out[0]->vec, in[0]->vec, in[1]->vec);
	else {
		/* make output size of input image. */
		CompBuf *cbuf= in[0]->data;
		CompBuf *stackbuf= alloc_compbuf(cbuf->x, cbuf->y, CB_RGBA, 1);

		composit2_pixel_processor(node, stackbuf, cbuf, in[0]->vec, in[1]->data, in[1]->vec, do_gain, CB_RGBA, CB_VAL);
		composit2_pixel_processor(node, stackbuf, stackbuf, in[0]->vec, in[1]->data, in[2]->vec, do_gain_red, CB_RGBA, CB_VAL);
		composit2_pixel_processor(node, stackbuf, stackbuf, in[0]->vec, in[1]->data, in[3]->vec, do_gain_green, CB_RGBA, CB_VAL);
		composit2_pixel_processor(node, stackbuf, stackbuf, in[0]->vec, in[1]->data, in[4]->vec, do_gain_blue, CB_RGBA, CB_VAL);

		out[0]->data= stackbuf;
	}
}

bNodeType cmp_node_gain= {
	/* next, prev  */ NULL, NULL,
	/* type code   */ CMP_NODE_GAIN,
	/* name        */ "Gain",
	/* width+range */ 140, 100, 320,
	/* class+opts  */ NODE_CLASS_OP_COLOR, NODE_OPTIONS,
	/* input sock  */ cmp_node_gain_in,
	/* output sock */ cmp_node_gain_out,
	/* storage     */ "",
	/* execfunc    */ node_composit_exec_gain,
	/* butfunc     */ NULL,
	/* initfunc    */ NULL,
	/* freestoragefunc */ NULL,
	/* copystoragefunc */ NULL,
	/* id          */ NULL
};

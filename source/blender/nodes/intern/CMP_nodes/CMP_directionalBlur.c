#include "../CMP_util.h"


/* ************** Directional Blur ************** */
static bNodeSocketType cmp_node_directionalblur_in[]= {
	{ SOCK_RGBA, 1, "Image", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f },
	{ SOCK_VALUE, 1, "Factor", 1, 0, 0, 0, 0, 500 },
	{ SOCK_VALUE, 1, "Angle", 50.0, 0.0, 0.0, 0.0, -500, 500 },
	{ -1, 0, "" }
};

static bNodeSocketType cmp_node_directionalblur_out[]= {
	{ SOCK_RGBA, 0, "Image", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static void node_composit_exec_directionalblur(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	if(in[0]->data) {
		CompBuf *cbuf= in[0]->data;
		CompBuf *stackbuf= alloc_compbuf(cbuf->x, cbuf->y, cbuf->type, 1);
		int i, j, src_pix, src_width, src_height, srcydelt, x, y;
		int dirx, diry, step;
		float sumR= 0.0f, sumG= 0.0f, sumB= 0.0f, blur_factor, size= 0;
		double angle;
		float *srcpix, *srcdestpix, *outpix;

		src_pix= cbuf->type;
		src_width= cbuf->x;
		src_height= cbuf->y;
		srcpix= cbuf->rect;
		outpix= stackbuf->rect;
		srcdestpix= outpix;
		srcydelt= src_width*src_pix;

		step= (int)node->custom1;
		if(step<1)
			step= 1;

		/* if Bypass off */
		blur_factor= (int)(in[1]->vec[0]);
		if(blur_factor>0) {
			angle= (int)(in[2]->vec[0]);
			for(y= 0; y<src_height; y++) {
				for(x= 0; x<src_width; x++) {
					for(j= -blur_factor; j<blur_factor; j+=step) {
						size= (blur_factor*2);
						srcpix= cbuf->rect;
						dirx= (double)(x + j*cos((2*3.1415)/(360/angle)));
						diry= (double)(y + j*sin((2*3.1415)/(360/angle)));

						dirx= (dirx<0) ? 0 : ((dirx>=src_width) ? src_width-1 : dirx);
						diry= (diry<0) ? 0 : ((diry>=src_height) ? src_height-1 : diry);

						srcpix+= diry*srcydelt + dirx*src_pix;
						sumR+= srcpix[0];
						sumG+= srcpix[1];
						sumB+= srcpix[2];
					}
					outpix= stackbuf->rect;
					outpix+= y*srcydelt + x*src_pix;
					outpix[0]= sumR/(size/(float)step);
					outpix[1]= sumG/(size/(float)step);
					outpix[2]= sumB/(size/(float)step);
					sumR= sumG= sumB= 0;
				}
			}
		}
		else {
			for(y= 0; y<src_height; y++) {
				for(x= 0; x<src_width; x++) {
					for(i= 0; i<src_pix; i++) {
						outpix[i]= srcpix[i];
					}
					outpix+= src_pix;
					srcpix+= src_pix;
				}
			}
		}
		out[0]->data= stackbuf;
	}
}

bNodeType cmp_node_directionalblur= {
	/* next, prev  */ NULL, NULL,
	/* type code   */ CMP_NODE_DIRECTIONAL_BLUR,
	/* name        */ "Directional Blur",
	/* width+range */ 140, 100, 320,
	/* class+opts  */ NODE_CLASS_OP_FILTER, NODE_OPTIONS,
	/* input sock  */ cmp_node_directionalblur_in,
	/* output sock */ cmp_node_directionalblur_out,
	/* storage     */ "",
	/* execfunc    */ node_composit_exec_directionalblur,
	/* butfunc     */ NULL,
	/* initfunc    */ NULL,
	/* freestoragefunc */ NULL,
	/* copystoragefunc */ NULL,
	/* id          */ NULL
};

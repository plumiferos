/**
 *
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * The Original Code is Copyright (C) 2008 Blender Foundation.
 * All rights reserved.
 *
 * The Original Code is: all of this file.
 *
 * Contributor(s): none yet.
 *
 * ***** END GPL LICENSE BLOCK *****
 */

#include "../CMP_util.h"


#define REORDER_RED 0
#define REORDER_GREEN 1
#define REORDER_BLUE 2
#define REORDER_ALPHA 3
#define REORDER_NONE 4

/* **************** Reorder ******************** */
static bNodeSocketType cmp_node_reorder_in[]= {
	{ SOCK_RGBA, 1, "Image", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f},
	{ -1, 0, "" }
};

static bNodeSocketType cmp_node_reorder_out[]= {
	{ SOCK_RGBA, 1, "Image", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
	{ -1, 0, "" }
};

void do_reorder(bNode *node, float *out, float *in, float *fac)
{
	NodeReorder *nr= (NodeReorder *)node->storage;

	if (nr->ch0 == REORDER_NONE)
		out[0]= 0.0f;
	else
		out[0]= in[nr->ch0];

	if (nr->ch1 == REORDER_NONE)
		out[1]= 0.0f;
	else
		out[1]= in[nr->ch1];

	if (nr->ch2 == REORDER_NONE)
		out[2]= 0.0f;
	else
		out[2]= in[nr->ch2];

	if (nr->ch3 == REORDER_NONE)
		out[3]= 0.0f;
	else
		out[3]= in[nr->ch3];
}

static void node_composit_exec_reorder(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	CompBuf *cbuf;
	CompBuf *stackbuf;

	if (!in[0]->data)
		return;


	cbuf= in[0]->data;
	stackbuf= alloc_compbuf(cbuf->x, cbuf->y, CB_RGBA, 1);
	composit2_pixel_processor(node, stackbuf, cbuf, in[0]->vec, in[0]->data, NULL, do_reorder, CB_RGBA, 0);
	out[0]->data= stackbuf;
}

static void node_composit_init_reorder(bNode* node)
{
	NodeReorder *nr= MEM_callocN(sizeof(NodeReorder), "node reorder");

	node->storage= nr;
	nr->ch0= REORDER_RED;
	nr->ch1= REORDER_GREEN;
	nr->ch2= REORDER_BLUE;
	nr->ch3= REORDER_ALPHA;
}

bNodeType cmp_node_reorder= {
	/* *next,*prev */ NULL, NULL,
	/* type code   */ CMP_NODE_REORDER,
	/* name        */ "Reorder",
	/* width+range */ 150, 120, 160,
	/* class+opts  */ NODE_CLASS_CONVERTOR, NODE_OPTIONS,
	/* input sock  */ cmp_node_reorder_in,
	/* output sock */ cmp_node_reorder_out,
	/* storage     */ "NodeReorder", 
	/* execfunc    */ node_composit_exec_reorder,
	/* butfunc     */ NULL,
	/* initfunc    */ node_composit_init_reorder,
	/* freestoragefunc */ node_free_standard_storage,
	/* copystoragefunc */ node_copy_standard_storage,
	/* id          */ NULL
};

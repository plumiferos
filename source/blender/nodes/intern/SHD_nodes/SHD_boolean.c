/*
 * Signed-off-by: Diego Hernan Borghetti <bdiego@gmail.com>
 */

#include "../SHD_util.h"


/* ************** Boolean ************** */
static bNodeSocketType sh_node_boolean_in[]= {
	{ SOCK_VALUE, 1, "Limit", 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f },
	{ SOCK_RGBA, 1, "Color1", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f },
	{ SOCK_RGBA, 1, "Color2", 0.8f, 0.8f, 0.8f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static bNodeSocketType sh_node_boolean_out[]= {
	{ SOCK_RGBA, 0, "Color", 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },
	{ -1, 0, "" }
};

static void node_shader_exec_boolean(void *data, bNode *node, bNodeStack **in, bNodeStack **out)
{
	/* stack order in: fac, col1, col2 */
	float fac;
	float col1[3], col2[3];

	nodestack_get_vec(&fac, SOCK_VALUE, in[0]);
	CLAMP(fac, 0.0f, 1.0f);

	nodestack_get_vec(col1, SOCK_VECTOR, in[1]);
	nodestack_get_vec(col2, SOCK_VECTOR, in[2]);

	boolean_operation(node->custom1, out[0]->vec, fac, col1, col2);
}

bNodeType sh_node_boolean= {
	/* next, prev  */ NULL, NULL,
	/* type code   */ SH_NODE_BOOLEAN,
	/* name        */ "Boolean",
	/* width+range */ 80, 40, 120,
	/* class+opts  */ NODE_CLASS_CONVERTOR, NODE_OPTIONS,
	/* input sock  */ sh_node_boolean_in,
	/* output sock */ sh_node_boolean_out,
	/* storage     */ "",
	/* execfunc    */ node_shader_exec_boolean,
	/* butfunc     */ NULL,
	/* initfunc    */ NULL,
	/* freestoragefunc */ NULL,
	/* copystoragefunc */ NULL,
	/* id          */ NULL
};

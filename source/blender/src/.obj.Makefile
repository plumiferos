#
# $Id: .obj.Makefile 2 2002-10-12 11:37:38Z hans $
#

SDIR = $(HOME)/develop/source/blender/src

all debug clean:
	@echo "****> Object Makefile, chdir to $(SDIR) ..."
	@$(MAKE) -C $(SDIR) $@ || exit 1;

# $Id: SCA_MouseSensor.py 2607 2004-05-30 11:09:46Z kester $
# Documentation for SCA_MouseSensor
from SCA_ISensor import *

class SCA_MouseSensor(SCA_ISensor):
	"""
	Mouse Sensor logic brick.
	"""

	def getXPosition():
		"""
		Gets the x coordinate of the mouse.
		
		@rtype: integer
		@return: the current x coordinate of the mouse, in frame coordinates (pixels)
		"""
	def getYPosition():
		"""
		Gets the y coordinate of the mouse.
		
		@rtype: integer
		@return: the current y coordinate of the mouse, in frame coordinates (pixels).
		"""	

# $Id: SCA_ORController.py 2615 2004-06-02 12:43:27Z kester $
# Documentation for SCA_ORController
from SCA_IController import *

class SCA_ORController(SCA_IController):
	"""
	An OR controller activates when any connected sensor activates.
	
	There are no special python methods for this controller.
	"""


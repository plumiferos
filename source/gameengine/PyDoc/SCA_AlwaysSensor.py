# $Id: SCA_AlwaysSensor.py 2607 2004-05-30 11:09:46Z kester $
# Documentation for SCA_AlwaysSensor
from SCA_ISensor import *

class SCA_AlwaysSensor(SCA_ISensor):
	"""
	This sensor is always activated.
	"""


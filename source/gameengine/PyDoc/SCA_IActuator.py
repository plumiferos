# $Id: SCA_IActuator.py 2615 2004-06-02 12:43:27Z kester $
# Documentation for SCA_IActuator
from SCA_ILogicBrick import *

class SCA_IActuator(SCA_ILogicBrick):
	"""
	Base class for all actuator logic bricks.
	"""


# $Id: KX_VisibilityActuator.py 2615 2004-06-02 12:43:27Z kester $
# Documentation for KX_VisibilityActuator
from SCA_IActuator import *

class KX_VisibilityActuator(SCA_IActuator):
	"""
	Visibility Actuator.
	"""
	def set(visible):
		"""
		Sets whether the actuator makes its parent object visible or invisible.
		
		@param visible: - True: Makes its parent visible.
		                - False: Makes its parent invisible.
		"""

	
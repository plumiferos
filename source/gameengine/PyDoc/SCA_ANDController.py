# $Id: SCA_ANDController.py 2615 2004-06-02 12:43:27Z kester $
# Documentation for SCA_ANDController
from SCA_IController import *

class SCA_ANDController(SCA_IController):
	"""
	An AND controller activates only when all linked sensors are activated.
	
	There are no special python methods for this controller.
	"""


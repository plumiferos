# $Id: KX_NearSensor.py 2607 2004-05-30 11:09:46Z kester $
# Documentation for KX_NearSensor
from KX_TouchSensor import *

class KX_NearSensor(KX_TouchSensor):
	"""
	A near sensor is a specialised form of touch sensor.
	"""


# $Id: KX_SCA_EndObjectActuator.py 2615 2004-06-02 12:43:27Z kester $
# Documentation for KX_SCA_EndObjectActuator
from SCA_IActuator import *

class KX_SCA_EndObjectActuator(SCA_IActuator):
	"""
	Edit Object Actuator (in End Object mode)
	
	This actuator has no python methods.
	"""
	

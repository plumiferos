# $Id: SCA_IController.py 2615 2004-06-02 12:43:27Z kester $
# Documentation for KX_CameraActuator
from SCA_ILogicBrick import *

class SCA_IController(SCA_ILogicBrick):
	"""
	Base class for all controller logic bricks.
	"""


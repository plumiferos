//
// Add object to the game world on action of this actuator
//
// $Id: KX_SCA_EndObjectActuator.h 3271 2004-10-16 11:41:50Z kester $
//
// ***** BEGIN GPL/BL DUAL LICENSE BLOCK *****
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version. The Blender
// Foundation also sells licenses for use in proprietary software under
// the Blender License.  See http://www.blender.org/BL/ for information
// about this.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// The Original Code is Copyright (C) 2001-2002 by NaN Holding BV.
// All rights reserved.
//
// The Original Code is: all of this file.
//
// Contributor(s): none yet.
//
// ***** END GPL/BL DUAL LICENSE BLOCK *****
//
// Previously existed as:
// \source\gameengine\GameLogic\SCA_EndObjectActuator.h
// Please look here for revision history.

#ifndef __KX_SCA_ENDOBJECTACTUATOR
#define __KX_SCA_ENDOBJECTACTUATOR

#include "SCA_IActuator.h"

class SCA_IScene;

class KX_SCA_EndObjectActuator : public SCA_IActuator
{
	Py_Header;
	SCA_IScene*		m_scene;

 public:
	KX_SCA_EndObjectActuator(
		SCA_IObject* gameobj,
		SCA_IScene* scene,
		PyTypeObject* T=&Type
	);

	~KX_SCA_EndObjectActuator();

		CValue* 
	GetReplica(
	);

	virtual bool 
	Update();

	/* --------------------------------------------------------------------- */
	/* Python interface ---------------------------------------------------- */
	/* --------------------------------------------------------------------- */

	virtual PyObject*  
	_getattr(
		const STR_String& attr
	);
	
}; /* end of class KX_EditObjectActuator : public SCA_PropertyActuator */

#endif


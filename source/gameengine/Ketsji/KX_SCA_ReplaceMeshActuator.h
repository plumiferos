//
// Add object to the game world on action of this actuator
//
// $Id: KX_SCA_ReplaceMeshActuator.h 10371 2007-03-26 03:35:01Z snailrose $
//
// ***** BEGIN GPL/BL DUAL LICENSE BLOCK *****
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version. The Blender
// Foundation also sells licenses for use in proprietary software under
// the Blender License.  See http://www.blender.org/BL/ for information
// about this.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// The Original Code is Copyright (C) 2001-2002 by NaN Holding BV.
// All rights reserved.
//
// The Original Code is: all of this file.
//
// Contributor(s): none yet.
//
// ***** END GPL/BL DUAL LICENSE BLOCK *****
//
// Previously existed as:
// \source\gameengine\GameLogic\SCA_ReplaceMeshActuator.h
// Please look here for revision history.
//

#ifndef __KX_SCA_REPLACEMESHACTUATOR
#define __KX_SCA_REPLACEMESHACTUATOR

#include "SCA_IActuator.h"
#include "SCA_PropertyActuator.h"
#include "SCA_LogicManager.h"
#include "SCA_IScene.h"

#include "RAS_MeshObject.h"

class KX_SCA_ReplaceMeshActuator : public SCA_IActuator
{
	Py_Header;

	// mesh reference (mesh to replace)
	RAS_MeshObject* m_mesh;
	SCA_IScene*	 m_scene;

 public:
	KX_SCA_ReplaceMeshActuator(
		SCA_IObject* gameobj, 
		RAS_MeshObject *mesh, 
		SCA_IScene* scene,
		PyTypeObject* T=&Type
	);

	~KX_SCA_ReplaceMeshActuator(
	);

		CValue* 
	GetReplica(
	);

	virtual bool 
	Update();

	virtual PyObject*  
	_getattr(
		const STR_String& attr
	);
	void	InstantReplaceMesh();

	/* 1. setMesh */
	KX_PYMETHOD_DOC(KX_SCA_ReplaceMeshActuator,SetMesh);
	KX_PYMETHOD_DOC(KX_SCA_ReplaceMeshActuator,getMesh);
	KX_PYMETHOD_DOC(KX_SCA_ReplaceMeshActuator,instantReplaceMesh);

}; 

#endif

